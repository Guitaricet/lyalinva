import pickle
import logging
import argparse

import pandas as pd
from sklearn.svm import LinearSVR
from sklearn.model_selection import train_test_split

logging.getLogger().setLevel(logging.INFO)

parser = argparse.ArgumentParser()
parser.add_argument('--data', default='data/regression.csv')
parser.add_argument('--model', default='models/LinearSVR.sklearn')


if __name__ == '__main__':
    args = parser.parse_args()
    data = pd.read_csv(args.data)

    if data.columns[-1] == 'Y':
        data = data[data.columns[:-1]]

    with open(args.model, 'rb') as f:
        model = pickle.load(f)

    res = model.predict(data)
    logging.info('Predictions')
    logging.info(res)
