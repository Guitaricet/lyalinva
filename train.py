import pickle
import logging
import argparse

import pandas as pd
from sklearn.svm import LinearSVR
from sklearn.model_selection import train_test_split

logging.getLogger().setLevel(logging.INFO)

parser = argparse.ArgumentParser()
parser.add_argument('--data', default='data/regression.csv')
parser.add_argument('--saveas', default='LinearSVR')


def train_model_and_save(datapath, modelname):
    data = pd.read_csv(datapath)
    X = data[data.columns[:-1]]
    y = data[data.columns[-1]]

    model = LinearSVR(C=50)
    X_train, X_test, y_train, y_test = train_test_split(X, y)

    model.fit(X_train, y_train)
    r2 = model.score(X_test, y_test)
    logging.info('Test r2: {}'.format(r2))

    model_savepath = 'models/{}.sklearn'.format(modelname)
    with open(model_savepath, 'wb') as f:
        pickle.dump(model, f)

    logging.info('Model saved as {}'.format(model_savepath.split('/')[-1]))


if __name__ == '__main__':
    args = parser.parse_args()
    train_model_and_save(args.data, args.saveas)
