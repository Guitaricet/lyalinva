.. Software engineering for machine learning task documentation master file, created by
   sphinx-quickstart on Tue Mar 20 20:02:52 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Software engineering for machine learning task's documentation!
==========================================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
.. automodule:: statemachine
   :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
