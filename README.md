# Software engineering for machine learning tasks

## Requirements
Just run
pip install -r requirements.txt

## Scripts Documentation

Default values presented in square brackets

### train.py
```
    --data      training file path [data/regression]
    --saveas    model name with which it will be saved to models directory [LinearSVR.sklearn]
```

### predict.py
```
    --data      features file path [data/regression]
    --model     model path [models/LinearSVR.sklearn]
```
